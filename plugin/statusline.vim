if exists('loaded_statusline')
  finish
endif
let loaded_statusline = 1

" always show status line
set laststatus=2

" clear existing status line
set statusline=

" get buffer's current branch
function! GitBranchName()
  let l:path = expand("%:h")
  let l:branch = trim(system("git -C " . l:path . " branch --show-current 2>/dev/null"))

  return l:branch
endfunction

" automatically set branch name when opening a buffer
augroup GitBranch
  autocmd!
  au BufEnter * let b:git_branch = GitBranchName()
augroup END

" set initial state
let b:git_branch = GitBranchName()

" status line
set statusline+=%(\ [%{b:git_branch}]%) " repo branch name
set statusline+=\ %f                    " filename (relative) "
set statusline+=\ %m                    " modified flag (with []) "
set statusline+=%r                      " readonly flag (with []) "
set statusline+=%h                      " help flag (with []) "
set statusline+=%w                      " preview window flag (with [])"
set statusline+=%=%y                    " type of file (with []) "
set statusline+=\ (line
set statusline+=\ %l
set statusline+=/
set statusline+=%L
set statusline+=,
set statusline+=\ col
set statusline+=\ %c
set statusline+=)

